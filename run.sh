docker run -it --publish 6006:6006 --volume ${HOME}/tf_files:/tf_files --workdir /tf_files tensorflow/tensorflow:1.1.0 bash
curl -O https://raw.githubusercontent.com/tensorflow/tensorflow/r1.1/tensorflow/examples/image_retraining/retrain.py
tensorboard --logdir training_summaries &
python retrain.py   --bottleneck_dir=bottlenecks   --how_many_training_steps=500   --model_dir=inception   --summaries_dir=training_summaries/basic   --output_graph=retrained_graph.pb   --output_labels=retrained_labels.txt   --image_dir=dataset
curl -L https://goo.gl/3lTKZs > label_image.py
python label_image.py dataset/true/true1.JPG